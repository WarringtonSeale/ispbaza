import sqlite3
import pandas as pd

def createAndConnect(dbloc): #connects to database and returns a cursor and connection objects
    conn = sqlite3.connect(dbloc)
    print(sqlite3.version)
    print('connected to  sqlite3 database @  \'{} \''.format(dbloc))
    cur = conn.cursor()
    return cur, conn

def getData(dataloc, profID, angle): #gets data as pandas dataframe
    df = pd.read_csv(dataloc,
                     sep = '\t',
                     skiprows = 9,
                     names = ['time', 'cm', 'cd', 'cl', 'clf', 'clr']
                     ).set_index('time')
    df['profileID'] = profID
    df['angleOfAttack'] = angle
    return df

def createTableInDb(cur, tabname):
    command = 'create table {} (time int, profileID int, angleOfAttack double(10,10), cm double(40, 40), cd double(40, 40), cl double(40, 40), clf double(40, 40), clr double(40, 40))'.format(tabname)
    cur.execute(command)

def saveDfToTable(df, conn, tabname):
    df.to_sql(tabname, conn, if_exists='append')
    print('{} saved to database'.format(df))
