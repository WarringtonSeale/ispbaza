import functions
import time


angleOfAttack = '5'
profileID = 'NACA_0006'
databaseName = time.strftime('%Y-%M-%d--%H-%M-%S')
dataLocation = './data/0006/-5'


cur, con = functions.createAndConnect('./{}.db'.format(databaseName))
dataLocation = './data/0006/_5/coeffs.dat'
df0 = functions.getData(dataLocation, '0006', -5)
dataLocation = './data/0012/_5/coeffs.dat'
df1 = functions.getData(dataLocation, '0012', -5)
dataLocation = './data/0024/_5/coeffs.dat'
df2 = functions.getData(dataLocation, '0024', -5)
dataLocation = './data/0006/0/coeffs.dat'
df3 = functions.getData(dataLocation, '0006', 0)
dataLocation = './data/0012/0/coeffs.dat'
df4 = functions.getData(dataLocation, '0012', 0)
dataLocation = './data/0024/0/coeffs.dat'
df5 = functions.getData(dataLocation, '0024', 0)
dataLocation = './data/0006/5/coeffs.dat'
df6 = functions.getData(dataLocation, '0006', 5)
dataLocation = './data/0012/5/coeffs.dat'
df7 = functions.getData(dataLocation, '00012', 5)
dataLocation = './data/0024/5/coeffs.dat'
df8 = functions.getData(dataLocation, '0024', 5)
dfs = [df0, df1, df2, df3, df4, df5, df6, df7, df8]
functions.createTableInDb(cur, 'data')
for df in range(len(dfs)):
    functions.saveDfToTable(dfs[df], con, 'data')

###################################################
#functions.createTableInDb(cur, 'alpha0')
#functions.saveDfToTable(df, con, 'alpha0')
#functions.createTableInDb(cur, 'alpha_5')
#functions.saveDfToTable(df, con, 'alpha_5')
###################################################
