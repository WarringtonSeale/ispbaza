import sqlite3
import pandas as pd
import pylatex as pl
from random import randint
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
import seaborn as sns
from mpl_toolkits.mplot3d import Axes3D


def readDB(dbname):
    con = sqlite3.connect('./{}.db'.format(dbname))
    cur =con.cursor()
    df0 = pd.read_sql_query('select *  from data where time = (select max(time)from data where profileID = 0006 and angleOfAttack=-5) and profileID=0006 and angleOfAttack=-5;', con)
    df1 = pd.read_sql_query('select *  from data where time = (select max(time)from data where profileID = 0012 and angleOfAttack=-5) and profileID=0012 and angleOfAttack=-5;', con)
    df2 = pd.read_sql_query('select *  from data where time = (select max(time)from data where profileID = 0024 and angleOfAttack=-5) and profileID=0024 and angleOfAttack=-5;', con)
    df3 = pd.read_sql_query('select *  from data where time = (select max(time)from data where profileID = 0006 and angleOfAttack=0) and profileID=0006 and angleOfAttack=0;', con)
    df4 = pd.read_sql_query('select *  from data where time = (select max(time)from data where profileID = 0012 and angleOfAttack=0) and profileID=0012 and angleOfAttack=0;', con)
    df5 = pd.read_sql_query('select *  from data where time = (select max(time)from data where profileID = 0024 and angleOfAttack=0) and profileID=0024 and angleOfAttack=0;', con)
    df6 = pd.read_sql_query('select *  from data where time = (select max(time)from data where profileID = 0006 and angleOfAttack=5) and profileID=0006 and angleOfAttack=5;', con)
    df7 = pd.read_sql_query('select *  from data where time = (select max(time)from data where profileID = 0012 and angleOfAttack=5) and profileID=0012 and angleOfAttack=5;', con)
    df8 = pd.read_sql_query('select *  from data where time = (select max(time)from data where profileID = 0024 and angleOfAttack=5) and profileID=0024 and angleOfAttack=5;', con)

    return df0, df1, df2, df3, df4, df5, df6, df7, df8


bigstring = r'''[ \begin{cases}
K_1 \longrightarrow MAX\\
K_2 \geq 0.5\\
K_3 \leq 0.05\\
\end{cases}
\]'''

df0, df1, df2, df3, df4, df5, df6, df7, df8 = readDB('a') #zmienic na poprawna nazwe database
print(df0,'\n', df1,'\n', df2,'\n', df3,'\n', df4,'\n', df5,'\n', df6,'\n', df7,'\n', df8,'\n')

dfbig = df0.append(df1).append(df2).append(df3).append(df4).append(df5).append(df6).append(df7).append(df8).reset_index(drop=True)
print(dfbig)


sns.set(context='talk')# wykres planu eksperymentu OK
f, ax = plt.subplots(1, 1)
sns.scatterplot(x = dfbig['angleOfAttack'], y = (0, 1, 2, 0, 1, 2, 0, 1, 2))
ax.set(xlabel = '$\\alpha [\\degree]$', ylabel='')
ax.yaxis.set_major_formatter(ticker.IndexFormatter(['NACA0006', 'NACA0012', 'NACA024']))
ax.minorticks_off()
plt.xticks((-5, 0, 5))
plt.yticks((0, 1, 2))
f.savefig('plot1.pdf', bbox_inches='tight')



f, ax = plt.subplots(1, 1) #wykres cl od alfa
sns.lineplot(x = dfbig.loc[dfbig['profileID'] == 6]['angleOfAttack'], y = dfbig.loc[dfbig['profileID'] == 6]['cl']) ###<------------------ ZMIENIC NA LINEPLOTY JAK BEDA DOBRE DANE
sns.lineplot(x = dfbig.loc[dfbig['profileID'] == 12]['angleOfAttack'], y = dfbig.loc[dfbig['profileID'] == 12]['cl'])
sns.lineplot(x = dfbig.loc[dfbig['profileID'] == 24]['angleOfAttack'], y = dfbig.loc[dfbig['profileID'] == 24]['cl'])
plt.xticks((-5, 0, 5))
ax.set(xlabel = 'Kąt natarcia $\\alpha [\\degree]$', ylabel = 'Współczynnik siły nośnej $Cl\\;[-]$')
ax.legend(['NACA0006', 'NACA0012', 'NACA0024'])
f.savefig('plot2.pdf', bbox_inches='tight')

f, ax = plt.subplots(1, 1) #wykres cd od alfa
sns.lineplot(x = dfbig.loc[dfbig['profileID'] == 6]['angleOfAttack'], y = dfbig.loc[dfbig['profileID'] == 6]['cd']) ###<------------------ ZMIENIC NA LINEPLOTY JAK BEDA DOBRE DANE
sns.lineplot(x = dfbig.loc[dfbig['profileID'] == 12]['angleOfAttack'], y = dfbig.loc[dfbig['profileID'] == 12]['cd'])
sns.lineplot(x = dfbig.loc[dfbig['profileID'] == 24]['angleOfAttack'], y = dfbig.loc[dfbig['profileID'] == 24]['cd'])
plt.xticks((-5, 0, 5))
ax.set(xlabel = 'Kąt natarcia $\\alpha [\\degree]$', ylabel = 'Współczynnik siły oporu $Cd\\;[-]$')
ax.legend(['NACA0006', 'NACA0012', 'NACA0024'])
f.savefig('plot3.pdf', bbox_inches='tight')


f, ax = plt.subplots(1, 1) #wykres doskonalosci od alfa
sns.lineplot(x = dfbig.loc[dfbig['profileID'] == 6]['angleOfAttack'], y = (dfbig.loc[dfbig['profileID'] == 6]['cl']/dfbig.loc[dfbig['profileID'] == 6]['cd'])) ###<------------------ ZMIENIC NA LINEPLOTY JAK BEDA DOBRE DANE
sns.lineplot(x = dfbig.loc[dfbig['profileID'] == 12]['angleOfAttack'], y = (dfbig.loc[dfbig['profileID'] == 12]['cl']/dfbig.loc[dfbig['profileID'] == 12]['cd']))
sns.lineplot(x = dfbig.loc[dfbig['profileID'] == 24]['angleOfAttack'], y = (dfbig.loc[dfbig['profileID'] == 24]['cl']/dfbig.loc[dfbig['profileID'] == 24]['cd']))
plt.xticks((-5, 0, 5))
ax.set(xlabel = 'Kąt natarcia $\\alpha [\\degree]$', ylabel = 'Doskonałość aerodynamiczna $\\frac{Cl}{Cd}\\;[-]$')
ax.legend(['NACA0006', 'NACA0012', 'NACA0024'])
f.savefig('plot4.pdf', bbox_inches='tight')


f = plt.figure()
ax = plt.axes(projection='3d')
ax.scatter3D(xs = dfbig.loc[dfbig['profileID'] == 6]['cl'], ys = dfbig.loc[dfbig['profileID'] == 6]['cd'], zs = (dfbig.loc[dfbig['profileID'] == 6]['cl']/dfbig.loc[dfbig['profileID'] == 6]['cd']))
f.savefig('plot5.pdf', bbox_inches='tight')



def fill_report(doc):

    with doc.create(pl.section.Section('Konfiguracja optymalizacji')):
        with doc.create(pl.section.Subsection('Zmienne')):
            with doc.create(pl.Center()) as centered:
                with centered.create(pl.Tabu("X[r] X[r] X[r] X[r] X[r]", spread="1in")) as data_table:
                    header_row1 = ["Nazwa zmiennej", "Oznaczenie", "Dolna granica", "Górna granica", "Typ"]
                    data_table.add_row(header_row1, mapper=[pl.utils.bold])
                    data_table.add_hline()
                    data_table.add_row(["Kąt natarcia", pl.utils.NoEscape(r'$\alpha$'), "-5", "5", "ciągły"])
                    data_table.add_row(["Profil", 'pr', "NACA0006", "NACA0024", "dyskretny"])
            doc.append(pl.utils.NoEscape(r'\vspace{1cm}'))
        with doc.create(pl.section.Subsection('Kryteria optymalizacji')):
            with doc.create(pl.Center()) as centered:
                with centered.create(pl.Tabu("X[r] X[r] X[r] X[r]", spread="1in")) as data_table:
                    header_row1 = ["Kryterium", "Rodzaj kryterium", "Oznaczenie", "Kierunek optymalizacji"]
                    data_table.add_row(header_row1, mapper=[pl.utils.bold])
                    data_table.add_hline()
                    data_table.add_row(["Doskonałość aerodynamiczna (K1)","Główne", pl.utils.NoEscape(r'$\frac{Cl}{Cd}$'), "MAX"])
                    data_table.add_row(["Współczynnik siły nośnej (K2)","Drugorzędne", pl.utils.NoEscape(r'$Cl$'), "MAX"])
                    data_table.add_row(["Współczynnik oporu (K3)","Drugorzędne", pl.utils.NoEscape(r'$Cd$'), "MIN"])
                doc.append(pl.utils.NoEscape(r'\vspace{1cm}'))
                doc.append(pl.utils.NoEscape(bigstring))
                doc.append(pl.utils.NoEscape(r'\clearpage'))
        with doc.create(pl.section.Subsection('Plan eksperymentu')):
            with doc.create(pl.Itemize()) as itemize:
                itemize.add_item(pl.utils.NoEscape(r'Typ planu eksperymentu: plan całkowity $2^3$'))
                itemize.add_item('Liczba poziomów: 3')
                itemize.add_item('Liczba zmiennych: 2')
            doc.append(pl.utils.NoEscape(r'\vspace{1cm}'))
        with doc.create(pl.section.Subsection('Wartości zmiennych')):
            with doc.create(pl.Center()) as centered:
                with centered.create(pl.Tabu("X[r] X[r]", spread="1in")) as data_table:
                    header_row1 = ["Oznaczenie", "Wartość"]
                    data_table.add_row(header_row1, mapper=[pl.utils.bold])
                    data_table.add_hline()
                    data_table.add_row([pl.utils.NoEscape(r'$\alpha_0$'), pl.utils.NoEscape(r'\SI{-5}{\degree}')])
                    data_table.add_row([pl.utils.NoEscape(r'$\alpha_1$'), pl.utils.NoEscape(r'\SI{0}{\degree}')])
                    data_table.add_row([pl.utils.NoEscape(r'$\alpha_2$'), pl.utils.NoEscape(r'\SI{5}{\degree}')])
                    data_table.add_row([pl.utils.NoEscape(r'$pr_0$'), "NACA0006"])
                    data_table.add_row([pl.utils.NoEscape(r'$pr_1$'), "NACA0012"])
                    data_table.add_row([pl.utils.NoEscape(r'$pr_2$'), "NACA0024"])
                doc.append(pl.utils.NoEscape(r'\vspace{1cm}'))
                with doc.create(pl.Figure(position='h')) as figure:
                    figure.add_image('plot1', width=pl.utils.NoEscape(r'0.6\linewidth'))
                    figure.add_caption('Plan eksperymentu')
                doc.append(pl.utils.NoEscape(r'\clearpage'))
    with doc.create(pl.section.Section('Wyniki')):
        with doc.create(pl.section.Subsection('Współczynnik siły nośnej w zależności od kąta natarcia')):
            with doc.create(pl.Center()) as centered:
                with doc.create(pl.Figure(position='h')) as figure:
                    figure.add_image('plot2', width=pl.utils.NoEscape(r'0.45\linewidth'))
                    figure.add_caption('Współczynnik siły nośnej w zależności od kąta natarcia')
                doc.append(pl.utils.NoEscape(r'\vspace{1cm}'))
        with doc.create(pl.section.Subsection('Współczynnik siły oporu w zależności od kąta natarcia')):
            with doc.create(pl.Center()) as centered:
                with doc.create(pl.Figure(position='hb')) as figure:
                    figure.add_image('plot3', width=pl.utils.NoEscape(r'0.45\linewidth'))
                    figure.add_caption('Współczynnik siły oporu w zależności od kąta natarcia')
                doc.append(pl.utils.NoEscape(r'\clearpage'))
        with doc.create(pl.section.Subsection('Doskonałość aerodynamiczna w zależności od kąta natarcia')):
            with doc.create(pl.Center()) as centered:
                with doc.create(pl.Figure(position='h')) as figure:
                    figure.add_image('plot4', width=pl.utils.NoEscape(r'0.45\linewidth'))
                    figure.add_caption('Doskonałość aerodynamiczna w zależności od kąta natarcia')
            doc.append(pl.utils.NoEscape(r'\vspace{1cm}'))
        with doc.create(pl.section.Subsection('Optymalny profil i kąt natarcia')):
            with doc.create(pl.Itemize()) as itemize:
                itemize.add_item(pl.utils.NoEscape(r'Kąt natarcia: \SI{5}{\degree}'))
                itemize.add_item('Nazwa profilu: NACA0012')
                itemize.add_item('Doskonałość aerodynamiczna: ${} = max$'.format(np.round(max(dfbig.loc[dfbig['profileID'] == 12]['cl']/dfbig.loc[dfbig['profileID'] == 12]['cd']),5)))
                itemize.add_item(pl.utils.NoEscape(r'Współczynnik siły nośnej: ${} \geq 0.5$ '.format(max(dfbig.loc[dfbig['profileID'] == 12]['cl']))))
                itemize.add_item(pl.utils.NoEscape(r'Współczynnik siły oporu: ${} \leq 0.05$'.format(np.round(min(dfbig.loc[dfbig['profileID'] == 12]['cd']),5))))
            doc.append('Wszystkie kryteria zostały spełnione, profil jest optymalny')
doc = pl.document.Document('basic', inputenc = 'utf8', fontenc=None, documentclass='article', geometry_options='(8in, 11in)')

doc.preamble.append(pl.Command('title','Raport z wielokryterialnej optymalizacji profilu lotniczego'))
#doc.preamble.append(pl.Command('date', '31 stycznia 2020, 03:42 '))
doc.preamble.append(pl.Command('date', pl.utils.NoEscape(r'\today, \currenttime')))

doc.append(pl.utils.NoEscape(r'\maketitle'))
doc.packages.append(pl.Package('datetime'))
doc.packages.append(pl.Package('amsmath'))
doc.packages.append(pl.Package('polski'))
doc.packages.append(pl.Package('siunitx'))

fill_report(doc)

tex = doc.dumps()
print(tex)

doc.generate_pdf('report', clean_tex=False, silent=True)
